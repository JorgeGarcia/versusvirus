-- Set timezone to UTC
SET TIME ZONE 'UTC';

-- Enable Uuid
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Create migrations table.
CREATE TABLE IF NOT EXISTS migrations (
    current_migration SMALLINT NOT NULL DEFAULT 0 CHECK (current_migration >= 0)
);
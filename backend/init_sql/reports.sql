CREATE TABLE IF NOT EXISTS reports (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    feeling VARCHAR(10) NOT NULL,
    headache BOOLEAN DEFAULT NULL,
    cough BOOLEAN DEFAULT NULL,
    respiratory_problems BOOLEAN DEFAULT NULL,
    weakness BOOLEAN DEFAULT NULL,
    digestive_problems BOOLEAN DEFAULT NULL,
    lost_smell_taste BOOLEAN DEFAULT NULL,
    contact_with_positives CHAR(1) DEFAULT NULL,
    num_contacts SMALLINT DEFAULT NULL CHECK (num_contacts >= 0)
);
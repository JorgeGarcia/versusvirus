-- Added the created_on column for reports.
ALTER TABLE reports ADD created_on TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP;
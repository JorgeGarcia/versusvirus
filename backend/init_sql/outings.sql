CREATE TABLE IF NOT EXISTS outings (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    start_time TIMESTAMP WITH TIME ZONE NOT NULL,
    end_time TIMESTAMP WITH TIME ZONE NOT NULL,
    location VARCHAR(50) NOT NULL,
    food_cleaned BOOLEAN DEFAULT NULL,
    mask_used BOOLEAN DEFAULT NULL,
    gloves_used BOOLEAN DEFAULT NULL,
    contact_with_people BOOLEAN DEFAULT NULL,
    contact_with_positives CHAR(1) DEFAULT NULL,
    washed_hands VARCHAR(10) DEFAULT NULL
);
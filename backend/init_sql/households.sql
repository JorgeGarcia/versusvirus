CREATE TABLE IF NOT EXISTS households (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    return_code CHAR(7) NOT NULL UNIQUE,
    num_people SMALLINT NOT NULL CHECK (num_people >= 0 AND num_people < 50),
    postal_code VARCHAR(10) NOT NULL,
    country VARCHAR(10) NOT NULL
);
CREATE TABLE IF NOT EXISTS visits (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    start_time TIMESTAMP WITH TIME ZONE NOT NULL,
    end_time TIMESTAMP WITH TIME ZONE NOT NULL,
    what VARCHAR(10) NOT NULL,
    food_cleaned BOOLEAN DEFAULT NULL,
    direct_contact BOOLEAN NOT NULL
);
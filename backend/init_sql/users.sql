CREATE TABLE IF NOT EXISTS users (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    household_id uuid REFERENCES households(id) ON DELETE CASCADE,
    return_code CHAR(7) NOT NULL UNIQUE,
    age SMALLINT NOT NULL CHECK (age >= 0 AND age < 150),
    confirmed_positive BOOLEAN NOT NULL DEFAULT FALSE,
    smoke BOOLEAN NOT NULL DEFAULT FALSE,
    drugs BOOLEAN NOT NULL DEFAULT FALSE,
    immunity_problems BOOLEAN NOT NULL DEFAULT FALSE,
    respiratory_syndroms BOOLEAN NOT NULL DEFAULT FALSE,
    blood_type BOOLEAN NOT NULL DEFAULT FALSE,
    weight REAL DEFAULT NULL CHECK (weight > 2 AND weight < 1000)
);
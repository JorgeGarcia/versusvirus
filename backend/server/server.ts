// We load the configuration from a .env file, if it exists
import dotenv from 'dotenv';
dotenv.config();

import express, { Router } from 'express'
import { urlencoded, json } from 'body-parser'
import { db } from './db'
import { UpdateHousehold, NewHousehold, Household } from './db/model/household'
import { UpdateUser, NewUser, User } from './db/model/user'
import { NewReport } from './db/model/report'
import { NewOuting } from './db/model/outing'
import { NewVisit } from './db/model/visit'

// BASE SETUP
// =============================================================================


// define our app using express
var app: express.Application = express();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(urlencoded({ extended: true }));
app.use(json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router: express.Router = Router();              // get an instance of the express Router

export function isNull(obj: null | any): obj is null {
    return obj === null;
}

function isUUID(obj: string): boolean {
    return /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(obj);
}

// Frontend static files
app.use(express.static('../frontend/build'));

// e.g. localhost:8080/api/households/263f4a67-60e7-4a8f-828d-ce1f2f459fca
router.get('/households/:id', async function (req, res) {
    let uuid: string = req.params.id;
    // Check appropriateness of the UUID
    if (!isUUID(uuid)) {
        console.error("error: invalid input syntax for type UUID: " + uuid);
        res.status(400);
        res.json({
            error: "invalid input syntax for type UUID: " + uuid
        });
        return;
    }
    try {
        let household = await db.household.get(uuid);
        let users: User[] = await db.user.getByHousehold(uuid);

        if (isNull(household) || isNull(users)) {
            res.json(null)
        } else {
            let retInfo: any = household;
            retInfo.users = users.map(user => user.id);

            res.json(retInfo);
        }
    } catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});

// e.g. localhost:8080/api/households/263f4a67-60e7-4a8f-828d-ce1f2f459fca/users
router.get('/households/:id/users', async function (req, res) {
    let uuid: string = req.params.id;
    // Check appropriateness of the UUID
    if (!isUUID(uuid)) {
        console.log("error: invalid input syntax for type UUID: " + uuid);
        res.status(400);
        res.json({
            error: "invalid input syntax for type UUID: " + uuid
        });
        return;
    }
    try {
        let users: User[] = await db.user.getByHousehold(uuid);

        if (isNull(users)) {
            res.json(null);
        } else {
            res.json(users);
        }
    }
    catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});

// e.g. localhost:8080/api/households/263f4a67-60e7-4a8f-828d-ce1f2f459fca
router.put('/households/:id', async function (req, res) {
    let uuid: string = req.params.id;
    try {
        if (typeof req.body.id !== "string" || typeof req.body.num_people !== "number" || typeof req.body.postal_code !== "string" || typeof req.body.country !== "string") {
            console.error("some of the input data has incorrect type");
            res.status(400);
            res.json({
                error: "some of the input data has incorrect type"
            });
        }
        let h: UpdateHousehold = {
            id: req.body.id,
            num_people: req.body.num_people,
            postal_code: req.body.postal_code,
            country: req.body.country,
        };
        await db.household.update(h);
        res.status(200);

    } catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});

// e.g. localhost:8080/api/households
router.post('/households', async function (req, res) {
    try {
        if (typeof req.body.num_people !== "number" || typeof req.body.postal_code !== "string" || typeof req.body.country !== "string") {
            console.error("some of the input data has incorrect type");
            res.status(400);
            res.json({
                error: "some of the input data has incorrect type"
            });
            return;
        }
        let nh: NewHousehold = {
            num_people: req.body.num_people,
            postal_code: req.body.postal_code,
            country: req.body.country,
        };
        console.log('Going to create a household');
        let household: Household = await db.household.create(nh);
        console.log('household created');
        res.json(household);
        res.status(200);
    } catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});

// e.g. localhost:8080/api/users
router.post('/users', async function (req, res) {
    try {
        if (typeof req.body.household_id !== "string" || typeof req.body.age !== "number") {
            console.error("some of the input data has incorrect type");
            res.status(400);
            res.json({
                error: "some of the input data has incorrect type"
            });
        }
        let nu: NewUser = {
            household_id: req.body.household_id,
            age: req.body.age,
        }
        await db.user.create(nu);
    } catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});

// e.g. localhost:8080/api/users/263f4a67-60e7-4a8f-828d-ce1f2f459fca
router.put('/users/:id', async function (req, res) {
    let uuid: string = req.params.id;

    try {
        if (typeof req.body.id !== "string" || typeof req.body.age !== "number" || typeof req.body.confirmed_positive !== "boolean" ||
            typeof req.body.smoke !== "boolean" || typeof req.body.drugs !== "boolean" || typeof req.body.immunity_problems !== "boolean" || typeof req.body.respiratory_syndroms !== "boolean" || typeof req.body.blood_type !== "string" || req.body.weight !== "number") {
            console.error("some of the input data has incorrect type");
            res.status(400);
            res.json({
                error: "some of the input data has incorrect type"
            });
        }
        let u: UpdateUser = {
            id: req.body.id,
            age: req.body.age,
            confirmed_positive: req.body.confirmed_positive,
            smoke: req.body.smoke,
            drugs: req.body.drugs,
            immunity_problems: req.body.immunity_problems,
            respiratory_syndroms: req.body.respiratory_syndroms,
            blood_type: req.body.blood_type,
            weight: req.body.weight,
        };
        await db.user.update(u);
        res.status(200);

    } catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});

// e.g. localhost:8080/api/reports
router.post('/reports', async function (req, res) {
    try {
        if (typeof req.body.user_id !== "string" || typeof req.body.feeling !== "string" || typeof req.body.temperature !== "number" ||
            typeof req.body.headache !== "boolean" || typeof req.body.cough !== "boolean" || typeof req.body.respiratory_problems !== "boolean" ||
            typeof req.body.weakness !== "boolean" || req.body.digestive_problems !== "boolean" || req.body.lost_smell_taste !== "boolean" ||
            typeof req.body.contact_with_positives !== "boolean" || typeof req.body.num_contacts !== "number") {
            console.error("some of the input data has incorrect type");
            res.status(400);
            res.json({
                error: "some of the input data has incorrect type"
            });
        }
        let r: NewReport = {
            user_id: req.body.user_id,
            feeling: req.body.feeling,
            temperature: req.body.temperature,
            headache: req.body.headache,
            cough: req.body.cough,
            respiratory_problems: req.body.respiratory_problems,
            weakness: req.body.weakness,
            digestive_problems: req.body.digestive_problems,
            lost_smell_taste: req.body.lost_smell_taste,
            contact_with_positives: req.body.contact_with_positives,
            num_contacts: req.body.num_contacts,
        }
        await db.report.create(r);
    } catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});

// e.g. localhost:8080/api/outings
router.post('/outings', async function (req, res) {
    try {
        if (typeof req.body.user_id !== "string" || typeof req.body.start_time !== "string" || typeof req.body.end_time !== "string" ||
            typeof req.body.location !== "string" || typeof req.body.food_cleaned !== "boolean" || typeof req.body.mask_used !== "boolean" ||
            typeof req.body.gloves_used !== "boolean" || req.body.contact_with_people !== "boolean" || req.body.contact_with_positives !== "boolean" ||
            typeof req.body.washed_hands !== "string") {
            console.error("some of the input data has incorrect type");
            res.status(400);
            res.json({
                error: "some of the input data has incorrect type"
            });
        }
        let regexp: string = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?$";
        if (req.body.start_time.match(regexp) == null || req.body.end_time.match(regexp) == null) {
            console.error("time values introduces have incorrect type");
            res.status(400);
            res.json({
                error: "time values introduces have incorrect type"
            });
        }
        var o: NewOuting = {
            user_id: req.body.user_id,
            start_time: req.body.start_time,
            end_time: req.body.end_time,
            location: req.body.where,
            food_cleaned: req.body.food_cleaned,
            mask_used: req.body.mask_used,
            gloves_used: req.body.gloves_used,
            contact_with_people: req.body.contact_with_people,
            contact_with_positives: req.body.contact_with_positives,
            washed_hands: req.body.washed_hands,
        }
        await db.outing.create(o);
    } catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});

// e.g. localhost:8080/api/visits
router.post('/visits', async function (req, res) {
    try {
        if (typeof req.body.user_id !== "string" || typeof req.body.start_time !== "string" || typeof req.body.end_time !== "string" ||
            typeof req.body.what !== "string" || typeof req.body.food_cleaned !== "boolean" || typeof req.body.direct_contact !== "boolean") {
            console.error("some of the input data has incorrect type");
            res.status(400);
            res.json({
                error: "time values introduces have incorrect type"
            });
        }
        let regexp: string = "^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?$";
        if (req.body.start_time.match(regexp) == null || req.body.end_time.match(regexp) == null) {
            console.error("some of the input data has incorrect type");
            res.status(400);
            res.json({
                error: "time values introduces have incorrect type"
            });
        }
        let v: NewVisit = {
            user_id: req.body.user_id,
            start_time: req.body.start_time,
            end_time: req.body.end_time,
            what: req.body.what,
            food_cleaned: req.body.food_cleaned,
            direct_contact: req.body.direct_contact,
        }
        await db.visit.create(v);
    } catch (ex) {
        console.error(ex);
        res.status(500);
        res.json({
            error: "Internal Server Error"
        });
    }
});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER AND DATABASE
// =============================================================================
try {
    db.init().then(function () {
        app.listen(port, function () {
            console.log('Looking for viruses at http://localhost:' + port + '/api');
        });
    })
}
catch (ex) {
    console.error(ex);
    console.error("DB initialization error. Something went wrong.");
}
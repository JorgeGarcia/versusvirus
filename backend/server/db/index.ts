import pgPromise from 'pg-promise'
import { IConnectionParameters } from 'pg-promise/typescript/pg-subset';
import { IInitOptions, IDatabase, IMain } from 'pg-promise'
import { IExtensions, UserModel, HouseholdModel, ReportModel, OutingModel, VisitModel, sql, runMigrations } from './model'

type ExtendedProtocol = IDatabase<IExtensions> & IExtensions;

// pg-promise initialization options:
const initOptions: IInitOptions<IExtensions> = {

    // Extending the database protocol with our custom repositories;
    // API: http://vitaly-t.github.io/pg-promise/global.html#event:extend
    extend(db: ExtendedProtocol, dc: any) {
        // Database Context (dc) is mainly needed for extending multiple databases with different access API.

        // Do not use 'require()' here, because this event occurs for every task and transaction being executed,
        // which should be as fast as possible.
        db.user = new UserModel(db, pgp);
        db.household = new HouseholdModel(db, pgp);
        db.report = new ReportModel(db, pgp);
        db.outing = new OutingModel(db, pgp);
        db.visit = new VisitModel(db, pgp);

        db.init = async () => {
            await db.none(sql.init);

            await db.household.init();
            await db.user.init();
            await db.report.init();
            await db.outing.init();
            await db.visit.init();

            console.log('Database initialized!');

            await runMigrations(db, pgp);

            console.log('All database migrations applied!');
            return Promise.resolve(null);
        }
    }
};

// Initializing the library:
const pgp: IMain = pgPromise(initOptions)

const dbConfig: IConnectionParameters = {
    host: process.env.DB_HOST || 'localhost',
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT && parseInt(process.env.DB_PORT) || 5432,
};

// Creating the database instance with extensions:
const db: ExtendedProtocol = pgp(dbConfig)

export { db, pgp }
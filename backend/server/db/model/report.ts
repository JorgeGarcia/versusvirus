import { IDatabase, IMain } from 'pg-promise';
import { sql } from '.';

export interface NewReport {
    user_id: string,
    feeling: string,
    temperature?: number,
    headache?: boolean,
    cough?: boolean,
    respiratory_problems?: boolean,
    weakness?: boolean,
    digestive_problems?: boolean,
    lost_smell_taste?: boolean,
    contact_with_positives?: string,
    num_contacts?: number,
}

export class ReportModel {
    constructor(private db: IDatabase<any>, private pgp: IMain) { }

    async init(): Promise<null> {
        return this.db.none(sql.reports);
    }

    async create(report: NewReport): Promise<null> {
        // TODO: check optional fields
        return this.db.none(
            'INSERT INTO reports(user_id, feeling, temperature, headache, cough, respiratory_problems, ' +
            'weakness, digestive_problems, lost_smell_taster, contact_with_positives, num_contacts) ' +
            'VALUES(${user_id}, ${feeling}, ${temperature}, ${headache}, ${cough}, ${respiratory_problems}, ' +
            '${weakness}, ${digestive_problems}, ${lost_smell_taster}, ${contact_with_positives}, ${num_contacts})',
            report
        );
    }
}
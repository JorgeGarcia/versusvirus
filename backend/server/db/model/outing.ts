import { IDatabase, IMain } from 'pg-promise';
import { sql } from '.';

export interface NewOuting {
    user_id: string,
    start_time: string,
    end_time: string,
    location: string,
    food_cleaned?: boolean,
    mask_used?: boolean,
    gloves_used?: boolean,
    contact_with_people?: boolean,
    contact_with_positives?: string,
    washed_hands?: string,
}

export class OutingModel {
    constructor(private db: IDatabase<any>, private pgp: IMain) { }

    async init(): Promise<null> {
        return this.db.none(sql.outings);
    }

    async create(outing: NewOuting): Promise<null> {
        // TODO: check optional fields
        return this.db.none(
            'INSERT INTO reports(user_id, start_time, end_time, location, food_cleaned, mask_used, gloves_used, ' +
            'contact_with_people, contact_with_positives, washed_hands) ' +
            'VALUES(${user_id}, ${start_time}, ${end_time}, ${location}, ${food_cleaned}, ${mask_used}, ' +
            '${gloves_used}, ${contact_with_people}, ${contact_with_positives}, washed_hands)',
            outing
        );
    }
}

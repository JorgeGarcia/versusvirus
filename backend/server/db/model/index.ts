import { UserModel } from "./user"
import { HouseholdModel } from "./household"
import { ReportModel } from "./report"
import { OutingModel } from "./outing"
import { VisitModel } from "./visit"
import { IDatabase, IMain, QueryFile, IQueryFileOptions } from "pg-promise"
import { isNull } from "../../server"
import path from 'path'
import fs from 'fs'
import util from 'util'

export interface IExtensions {
    init(): Promise<null>,

    user: UserModel,
    household: HouseholdModel,
    report: ReportModel,
    outing: OutingModel,
    visit: VisitModel,
}

export const sql = {
    init: read_sql("init.sql"),
    households: read_sql("households.sql"),
    users: read_sql("users.sql"),
    reports: read_sql("reports.sql"),
    outings: read_sql("outings.sql"),
    visits: read_sql("visits.sql"),
};

///////////////////////////////////////////////
// Helper for linking to external query files;
function read_sql(file: string): QueryFile {

    const fullPath: string = path.join('../init_sql', file); // generating full path;

    const options: IQueryFileOptions = {

        // minifying the SQL is always advised;
        // see also option 'compress' in the API;
        minify: true

        // See also property 'params' for two-step template formatting
    };

    const qf: QueryFile = new QueryFile(fullPath, options);

    if (qf.error) {
        // Something is wrong with our query file :(
        // Testing all files through queries can be cumbersome,
        // so we also report it here, while loading the module:
        console.error(qf.error);
    }

    return qf;

    // See QueryFile API:
    // http://vitaly-t.github.io/pg-promise/QueryFile.html
}

function returnCode(length: number): string {
    var result = '';
    var characters = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

const readdir = util.promisify(fs.readdir);

async function runMigrations(db: IDatabase<any>, pgp: IMain): Promise<null> {
    let currentMigration = await db.oneOrNone('SELECT current_migration FROM migrations;');
    if (isNull(currentMigration)) {
        await db.none("INSERT INTO migrations (current_migration) VALUES (0);");
        currentMigration = 0;
    } else {
        currentMigration = currentMigration.current_migration;
    }
    const migrationsFolder = './init_sql/migrations';

    let files = await readdir(migrationsFolder);
    files.sort((a: string, b: string) => {
        let aMigNum = parseInt(a.substr(0, 4));
        let bMigNum = parseInt(a.substr(0, 4));

        return aMigNum - bMigNum;
    });

    for (let i in files) {
        let file = files[i];

        let migNum = parseInt(file.substr(0, 4));
        if (migNum > currentMigration) {
            await db.none(read_sql('migrations/' + file));

            currentMigration = migNum;
            await db.none("UPDATE migrations SET current_migration = $1;", [migNum]);
        }
    }
    return Promise.resolve(null);
}

export { UserModel, HouseholdModel, ReportModel, OutingModel, VisitModel, returnCode, runMigrations }
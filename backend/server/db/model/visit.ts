import { IDatabase, IMain } from 'pg-promise';
import { sql } from '.';

export interface NewVisit {
    user_id: string,
    start_time: string,
    end_time: string,
    what: string,
    food_cleaned?: boolean,
    direct_contact: boolean,
}

export class VisitModel {
    constructor(private db: IDatabase<any>, private pgp: IMain) { }

    async init(): Promise<null> {
        return this.db.none(sql.visits);
    }

    async create(visit: NewVisit): Promise<null> {
        // TODO: check optionals
        return this.db.none(
            'INSERT INTO visits(user_id, start_time, end_time, what, food_cleaned, direct_contant) ' +
            'VALUES(${user_id}, ${start_time}, ${end_time}, ${what}, ${food_cleaned}, ${direct_contact})',
            visit
        );
    }
}

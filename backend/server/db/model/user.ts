import { IDatabase, IMain } from 'pg-promise';
import { sql, returnCode } from '.'

export interface User {
    id: string,
    household_id: string,
    return_code: string,
    age: number,
    confirmed_positive: boolean | null,
    smoke: boolean | null,
    drugs: boolean | null,
    immunity_problems: boolean | null,
    respiratory_syndroms: boolean | null,
    blood_type: string | null,
    weight: number | null,
}

export interface NewUser {
    household_id: string,
    age: number,
}

export interface UpdateUser {
    id: string,
    age?: number,
    confirmed_positive?: boolean,
    smoke?: boolean,
    drugs?: boolean,
    immunity_problems?: boolean,
    respiratory_syndroms?: boolean,
    blood_type?: string,
    weight?: number,
}

export class UserModel {
    constructor(private db: IDatabase<any>, private pgp: IMain) { }

    async init(): Promise<null> {
        return this.db.none(sql.users);
    }

    async create(user: NewUser): Promise<User> {
        let dbInput: any = user;
        dbInput.return_code = returnCode(7);

        return this.db.one('INSERT INTO users(household_id, return_code, age) ' +
            'VALUES(${household_id}, ${return_code}, ${age}) RETURNING *',
            user);
    }

    async update(user: UpdateUser): Promise<null> {
        // TODO: check optional fields
        return this.db.none('UPDATE users SET age = ${age}, confirmed_positive = ${confirmed_positive}, ' +
            'smoke = ${smoke}, immunity_problems = ${immunity_problems}, respiratory_syndroms = ${respiratory_syndroms' +
            'blood_type = ${blood_type}, weight = ${weight} WHERE id = ${id}',
            user);
    }

    async get(userId: string): Promise<User | null> {
        return this.db.oneOrNone('SELECT * FROM users WHERE id = $1', [userId]);
    }

    async getByReturnCode(returnCode: string): Promise<User | null> {
        return this.db.oneOrNone('SELECT * FROM users WHERE return_code = $1', [returnCode]);
    }

    async getByHousehold(householdId: string): Promise<User[]> {
        return this.db.any('SELECT * FROM users WHERE household_id = $1', [householdId]);
    }
}
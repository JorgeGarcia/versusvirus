import { IDatabase, IMain } from 'pg-promise';
import { sql, returnCode } from '.';

export interface Household {
    id: string,
    return_code: string,
    num_people: number,
    postal_code: string,
    country: string,
}

export interface NewHousehold {
    num_people: number,
    postal_code: string,
    country: string,
}

export interface UpdateHousehold {
    id: string,
    num_people?: number,
    postal_code?: string,
    country?: string,
}

export class HouseholdModel {
    constructor(private db: IDatabase<any>, private pgp: IMain) { }

    async init(): Promise<null> {
        return this.db.none(sql.households);
    }

    async create(household: NewHousehold): Promise<Household> {
        let dbInput: any = household;
        dbInput.return_code = returnCode(7);

        return this.db.one('INSERT INTO households(return_code, num_people, postal_code, country) ' +
            'VALUES(${return_code}, ${num_people}, ${postal_code}, ${country}) RETURNING *',
            dbInput);
    }

    async update(household: UpdateHousehold): Promise<null> {
        // TODO: check optional fields
        return this.db.none('UPDATE households SET num_people = ${num_people}, postal_code = ${postal_code}, ' +
            'country = ${country} WHERE id = ${id}',
            household);
    }

    async get(householdId: string): Promise<Household | null> {
        return this.db.oneOrNone('SELECT * FROM households WHERE id = $1', [householdId]);
    }

    async getByReturnCode(returnCode: string): Promise<Household | null> {
        return this.db.oneOrNone('SELECT * FROM household WHERE return_code = $1', [returnCode]);
    }
}
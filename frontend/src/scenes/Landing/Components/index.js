import React, { Component } from 'react';

import PersonalInfoStep from './PersonalInfoStep';
import HouseCodeStep from './HouseCodeStep';
import UserSymptomsStep from './UserSymptomsStep';
import ContactWithOthersStep from './ContactWithOthersStep';
import CreateHouseholdStep from './CreateHouseholdStep';
import EnterHouseCodeStep from './EnterHouseCodeStep';
import UserTestedStep from './UserTestedStep';
import ContactWithPositivesStep from './ContactWithPositivesStep';
import ShowHouseCodeStep from './ShowHouseCodeStep';
import FinalStep from './FinalStep';




class Landing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            step: 'personalInfo', // Control the current step in the wizard
            showNext: false
        };
        // Change handler: Will be called by every children of the wizard
        this.handleChange = this.handleChange.bind(this);

    }


    navigateTo = (nextStep) => {
        console.log(`Navigate to: ${nextStep}`)
        this.setState({
            step: nextStep
        })
    }

    handleChange(event) {
        const { name, value } = event.target
        this.setState({
            [name]: value,
            showNext: this.state.age && this.state.age.length > 1
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        let s = this.state
        const user = {
            name: s.username,
            age: s.age,
            positive: s.positive,
        }
        // TODO: use the service layer to send info back to the database

    }


    render() {
        return (
            <React.Fragment>
                <form onSubmit={this.handleSubmit}>

                    <PersonalInfoStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />

                    <HouseCodeStep
                        currentStep={this.state.step}
                        handleNext={this._next}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />

                    <EnterHouseCodeStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />

                    <CreateHouseholdStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />

                    <ShowHouseCodeStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />

                    <UserTestedStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />

                    <UserSymptomsStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />
                    <ContactWithOthersStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />

                    <ContactWithPositivesStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />

                    <FinalStep
                        currentStep={this.state.step}
                        handleChange={this.handleChange}
                        navigateTo={this.navigateTo}
                    />


                </form>
            </React.Fragment>
        );
    }
}


export default Landing;
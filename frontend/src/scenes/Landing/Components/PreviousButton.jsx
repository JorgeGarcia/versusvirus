import React, { Component } from 'react';

class PreviousButton extends Component {

    constructor(props) {
        super(props)
        this.state = {
            step: props.step
        }
    }

    render() {

        let step = this.state.step;
        if (step !== 1) {
            return (
                <button
                    className="btn btn-secondary"
                    type="button" onClick={this.props._prev}>
                    Go back
                </button>
            )
        } 
        return null;
    }
}

export default PreviousButton;
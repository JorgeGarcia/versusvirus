import React, { Component } from 'react';

class UserSymptomsStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
            sympthoms: '',
        }

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {
        // Check if we should draw the age and then call the handler in the parent
        const { name, value } = event.target
        this.setState({
            [name]: value,
        })

        this.props.handleChange(event)
    }


    navigateNext = () => {
        this.props.navigateTo('contact-with-others')
    }

    render() {
        // This is step "user-symptoms"
        if (this.props.currentStep !== 'user-symptoms') return null
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>Have you experienced any of these symptoms?<br /></h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-offset-3 col-xs-9 col-xs-offset-1 text-left">
                        <div className="checkbox"><label><input type="checkbox" />Temperature</label></div>
                        <div className="checkbox"><label><input type="checkbox" />Headache</label></div>
                        <div className="checkbox"><label><input type="checkbox" />Cough</label></div>
                        <div className="checkbox"><label><input type="checkbox" />Respiratory problems</label></div>
                        <div className="checkbox"><label><input type="checkbox" />Weakness</label></div>
                        <div className="checkbox"><label><input type="checkbox" />Not be able to smell or taste</label></div>
                        <div className="checkbox"><label><input type="checkbox" />Digestive problems</label></div>
                    </div>
                </div>
                <div className="row" style={{ marginTop: "2em" }}>
                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                        <button 
                            className="btn btn-primary btn-block" 
                            type="button" 
                            disabled=""
                            onClick={this.navigateNext}>
                            Next
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserSymptomsStep;
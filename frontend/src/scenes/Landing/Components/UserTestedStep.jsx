import React, { Component } from 'react';

class UserTestedStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
            testedPositive: props.positive || false,
        }

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {
        // Check if we should draw the age and then call the handler in the parent
        const { name, value } = event.target
        this.setState({
            [name]: value,
        })

        this.props.handleChange(event)
    }

    setAnswer = (evt) => {
        let isTested= evt.target.getAttribute('data-answer') === 'true'
        this.setState({ isTested: isTested })
        let navTo = isTested ? 'final' : 'user-symptoms'

        this.props.navigateTo(navTo)

    }

    navigateNext = () => {

        this.props.navigateTo('')
    }


    render() {
        // This is step "user-been-tested"
        if (this.props.currentStep !== 'user-been-tested') return null

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>Have you been tested positive?</h1>
                    </div>
                </div>
                <div className="row" style={{ marginTop: "2em" }}>
                    <div className="col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <button
                            className="btn btn-primary btn-block btn-lg"
                            type="button"
                            onClick={this.setAnswer}
                            data-answer={false}>
                            No
                        </button>
                        <div className="visible-xs-block visible-sm-block" style={{ marginTop: "2em" }}></div>
                    </div>
                    <div className="col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <button
                            className="btn btn-primary btn-block btn-lg"
                            type="button"
                            onClick={this.setAnswer}
                            data-answer={true}>
                            Yes
                        </button>
                    </div>
                </div>
            </div>
        )

    }

}

export default UserTestedStep;
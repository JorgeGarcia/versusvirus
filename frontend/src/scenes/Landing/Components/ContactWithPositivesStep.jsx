import React, { Component } from 'react';

class ContactWithPositivesStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
            testedPositive: props.positive || false,
        }

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {
        // Check if we should draw the age and then call the handler in the parent
        const { name, value } = event.target
        this.setState({
            [name]: value,
        })

        this.props.handleChange(event)
    }

    setAnswer = (evt) => {
        let isPositiveContacted= evt.target.getAttribute('data-answer') === 'true'
        this.setState({ isPositiveContacted: isPositiveContacted })
        let navTo = isPositiveContacted ? 'final' : 'final'

        this.props.navigateTo(navTo)

    }

    render() {
        // This is step 'contact-with-positives'
        if (this.props.currentStep !== 'contact-with-positives') return null

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>Any of them have been tested positive?<br /></h1>
                    </div>
                </div>
                <div className="row" style={{ marginTop: "2em" }}>
                    <div className="col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <button className="btn btn-primary btn-block btn-lg" type="button">No</button>
                        <div className="visible-xs-block visible-sm-block" style={{ marginTop: "2em" }}></div>
                    </div>
                    <div className="col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <button className="btn btn-primary btn-block btn-lg" type="button">Yes</button>
                    </div>
                </div>
            </div>
        )

    }

}

export default ContactWithPositivesStep;
import React, { Component } from 'react';

class HouseCodeStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
            housecode: props.housecode || '',
        }

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {
        // Check if we should draw the age and then call the handler in the parent
        const { name, value } = event.target
        this.setState({
            [name]: value,
        })

        this.props.handleChange(event)
    }

    setAnswer = (evt) => {
        let hasCode = evt.target.getAttribute('data-answer') === 'true'
        this.setState({ hasCode: hasCode })
        let navTo = hasCode ? 'enterHouseCode' : 'createHousehold'

        this.props.navigateTo(navTo)

    }

    render() {
        // This is step 2
        if (this.props.currentStep !== 'houseCode') { return null }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>Do you have already a household code?</h1>
                    </div>
                </div>
                <div className="row" style={{ marginTop: "2em" }}>
                    <div className="col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <button
                            className="btn btn-primary btn-block btn-lg"
                            type="button"
                            onClick={this.setAnswer}
                            data-answer={false}>
                            No
                            </button>
                        <div className="visible-xs-block visible-sm-block" style={{ marginTop: "2em" }}></div>
                    </div>
                    <div className="col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <button
                            className="btn btn-primary btn-block btn-lg"
                            type="button"
                            onClick={this.setAnswer}
                            data-answer={true}>
                            Yes
                        </button>
                    </div>
                </div>
            </div>
        )

    }

}

export default HouseCodeStep;
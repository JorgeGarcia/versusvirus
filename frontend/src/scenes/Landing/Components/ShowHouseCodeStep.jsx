import React, { Component } from 'react';

class ShowHouseCodeStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
            housecode: props.housecode || '',
        }

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {

        this.props.handleChange(event)
    }

    navigateNext = () => {
        this.props.navigateTo('user-been-tested')
    }


    render() {
        // This is step 'show-house-code'
        if (this.props.currentStep !== 'show-house-code') { return null }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>This is your house code<br /></h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <h1 className="text-danger" id="houseCode" style={{ fontSize: "3em" }}>ABC</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <h3>You can use this code to share your house with other people that live with you or to update your information from other device&nbsp;<br /></h3>
                    </div>
                </div>
                <div className="row" style={{ marginTop: "2em" }}>
                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                        <button 
                            className="btn btn-primary btn-block" 
                            type="button"
                            onClick={this.navigateNext}>
                                Got it!
                        </button>
                    </div>
                </div>
            </div>
        )

    }

}

export default ShowHouseCodeStep;
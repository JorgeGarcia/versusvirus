import React, { Component } from 'react';


class PersonalInfoStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
            name: props.name || '',
            age: props.age || '',
            showAge: false,
            showNext: false
        }
        console.log(`currentStep: ${this.state.currentStep}`)
        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {
        // Check if we should draw the age and then call the handler in the parent
        const { name, value } = event.target
        let start = this.state.age > 0; // Activate the button to continue
        this.setState({
            [name]: value,
            showAge: this.state.name !== '',
            showNext: start
        })

        this.props.handleChange(event)
    }

    navigateNext = () => {
        this.props.navigateTo('houseCode')
    }

    render() {

        if (this.props.currentStep !== 'personalInfo') {
            return null
        }
        return (
            // The markup for Step 1
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>Hello, how should we call you?</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <input
                            className="form-control"
                            id="name"
                            name="name"
                            type="text"
                            placeholder="Please write your name here"
                            value={this.props.username}
                            onChange={this.handleChange} // Callback to Wizard container
                        />
                    </div>
                </div>
                {this.state.showAge ?
                    <div>
                        <div className="row">
                            <div className="col-md-12">
                                <h2>What is your age?</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                                <input type="number"
                                    className="form-control"
                                    id="age"
                                    name="age"
                                    type="number"
                                    min="0"
                                    max="130"
                                    placeholder="Please enter your age here"
                                    value={this.state.age}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="row" style={{ marginTop: "2em" }}>
                            <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                                <button
                                    className="btn btn-primary btn-block"
                                    type="button"
                                    disabled={!this.state.showNext}
                                    onClick={this.navigateNext}>
                                    Let's get started
                                </button>
                            </div>
                        </div>
                    </div>
                    : null
                }
            </div>






        )
    }

}

export default PersonalInfoStep
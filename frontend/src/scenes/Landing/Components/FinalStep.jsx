import React, { Component } from 'react';


import wastedImg from './../../../img/cowid.jpg';

class FinalStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
        }

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {

        this.props.handleChange(event)
    }

    navigateNext = () => {
       // There is no next step
    }


    render() {
        // This is step 'final'
        if (this.props.currentStep !== 'final') { return null }

        return (
            <div>
                Under construction
                <div style={{backgroundImage: "url("+wastedImg+")", height: "100vh", width: "100vw", backgroundSize: "auto", backgroundRepeat: "no-repeat", backgroundPosition: "center"}}></div>
            </div>
        )

    }

}

export default FinalStep;
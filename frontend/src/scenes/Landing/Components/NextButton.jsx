import React, { Component } from 'react';

class NextButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            step: props.step,
            text: 'Continue'
        }
    }

    
    render() {

        let step = this.state.step;
        let text = step === 1 ? 'Let\'s get started' : 'Continue'
        if (step < 6) {
            return (
                <button
                    className="btn btn-primary float-right"
                    type="button" onClick={this.props.next}>
                    {text}
                </button>
            )
        }
        return null;

    }

}

export default NextButton;
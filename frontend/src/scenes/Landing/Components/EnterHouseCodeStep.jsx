import React, { Component } from 'react';

class EnterHouseCodeStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
            housecode: props.housecode || '',
        }

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {
       
        this.props.handleChange(event)
    }

    navigateNext = () => {
        this.props.navigateTo('user-been-tested')
    }


    render() {
        // This is step 'enterHouseCode'
        if (this.props.currentStep !== 'enterHouseCode') { return null }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>Please, enter your house code<br /></h1>
                    </div>
                </div>
                <div className="row" style={{ marginTop: "2em" }}>
                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <input 
                            type="text" 
                            className="form-control" 
                            autoFocus="" 
                            required="" 
                        />
                    </div>
                </div>
                <div className="row" style={{ marginTop: "2em" }}>
                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                        <button 
                            className="btn btn-primary btn-block" 
                            type="button" 
                            disabled=""
                            onClick={this.navigateNext}>
                            Next
                        </button>
                    </div>
                </div>
            </div>
        )

    }

}

export default EnterHouseCodeStep;
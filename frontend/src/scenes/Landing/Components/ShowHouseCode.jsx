import React, { Component } from 'react';

class ShowHouseCode extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: props.step,
            testedPositive: props.positive || false,
        }

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange = (event) => {
        // Check if we should draw the age and then call the handler in the parent
        const { name, value } = event.target
        this.setState({
            [name]: value,
        })

        this.props.handleChange(event)
    }

    render() {
        // This is step 'contact-with-positives'
        if (this.props.currentStep !== 'contact-with-possitives') return null
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>This is your house code<br /></h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <h1 className="text-danger" id="houseCode" style="font-size: 3em;">ABC</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <h3>You can use this code to share your house with other people that live with you or to update your information from other device&nbsp;<br /></h3>
                    </div>
                </div>
                <div className="row" style={{ marginTop: "2em" }}>
                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
                        <button className="btn btn-primary btn-block" type="button">Got it!</button>
                    </div>
                </div>
            </div>
        )

    }

}

export default ShowHouseCode;
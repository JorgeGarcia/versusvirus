import React from 'react';
import logo from './logo.svg';

import Landing from './scenes/Landing/Components'

import './App.css';

class App extends React.Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
            <Landing />
        </header>
      </div>
    );
  }
}

export default App;

